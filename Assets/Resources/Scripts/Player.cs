
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    [Header("Essentials")]
    static Player instance;
    InputControls _controls;
    bool isPlaying;
    bool isGameOver;
    float timer;

    [Header("Inner Variables")]
    float movement;
    [ShowInInspector]
    int contamination = 0;
    [SerializeField]
    int contaminationSpeed = 10;
    [SerializeField]
    int contaminationLimit = 100;
    float charge = 0;
    float chargeSpeed = 1f;
    float chargeLimit = 3f;

    [Header("Preferences")]
    [SerializeField]
    float movementSpeed = 10;

    [Header("Game Elements")]
    [SerializeField]
    Core core;
    [SerializeField]
    List<Image> chargeImages;

    [Header("Shield")]
    bool isShieldUp = false;
    float shieldTimer;
    [SerializeField]
    LineRenderer shieldLR;
    [SerializeField]
    Collider2D shieldCollider;

    [Header("Electrons")]
    [SerializeField]
    GameObject ElectronPrefab;
    [SerializeField]
    GameObject ElectronParent;
    float electronTimer = 0f;
    [SerializeField]
    float electronTimerLimit = 10f;
    [SerializeField]
    float electronSpawnRadius = 10f;

    public static Player GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;

        _controls = new InputControls();

        _controls.Action.Move.performed += ctx => movement = -ctx.ReadValue<Vector2>().x;
        _controls.Action.Move.canceled += ctx => movement = 0f;

        _controls.Action.Attack.performed += ctx => SpaceAction();

        _controls.Action.Mute.performed += ctx => MuteSound();

        shieldLR = GetComponent<LineRenderer>();
        shieldCollider = GetComponent<EdgeCollider2D>();
        isPlaying = false;
        isGameOver = false;
    }

    void Start()
    {
        ResetGame();
    }

    void ResetGame()
    {
        transform.SetPositionAndRotation(new Vector2(0, 3), Quaternion.identity);
        timer = 0f;
        electronTimer = 0f;
        isPlaying = false;
        isGameOver = false;
        movement = 0;
        charge = 0;
        contamination = 0;
        foreach (Image im in chargeImages)
        {
            im.fillAmount = 0f;
        }
    }

    private void Update()
    {
        if (isPlaying && !isGameOver)
        {
            timer += Time.deltaTime;
            electronTimer += Time.deltaTime;
            if (isShieldUp)
            {
                charge -= Time.deltaTime;
                if (charge <= 0)
                {
                    charge = 0;
                    switchShield(false);
                }
            }
            foreach (Image im in chargeImages)
            {
                im.fillAmount = Mathf.Lerp(im.fillAmount, ((float)charge / (float)chargeLimit), 20 * Time.deltaTime);
            }
            if (contamination >= contaminationLimit)
            {
                isPlaying = false;
                isGameOver = true;
                LoadDeathScreen();
            }
            else
            {
                if (electronTimer >= electronTimerLimit)
                {
                    electronTimer = 0f;
                    GenerateElectron();
                }
                UIManager.GetInstance().SetTimer(timer);
            }
        }
    }
    
    private void FixedUpdate()
    {
        if(movement != 0)
        {
            if (!isPlaying && !isGameOver)
            {
                isPlaying = true;
                UIManager.GetInstance().StartGame();
                core.StartGame();
            }
            Move();
        }
    }

    void Move()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, movement * movementSpeed * Time.deltaTime);
    }

    void SpaceAction()
    {
        if (!isPlaying && isGameOver)
        {
            isGameOver = false;
            UIManager.GetInstance().UnloadDeathScreen();
            ResetGame();
        }
        else if (isPlaying)
        {
            switchShield(true);
        }
    }

    public void Damage()
    {
        contamination += contaminationSpeed;
        core.Contaminate((float)contamination/ (float)contaminationLimit);
    }

    public void AddCharge()
    {
        if (!isShieldUp)
        {
            charge = Mathf.Clamp(charge + chargeSpeed, 0, chargeLimit);
        }
    }

    void switchShield(bool turnOn)
    {
        if(turnOn && !isShieldUp && charge > 0f)
        {
            isShieldUp = true;
            shieldCollider.enabled = true;
            AudioManager.GetInstance().PlayClip("shield");
            shieldLR.startWidth = 0.1f;
            shieldLR.endWidth = 0.1f;
        }
        else if (!turnOn && isShieldUp)
        {
            isShieldUp = false;
            AudioManager.GetInstance().StopClip("shield");
            shieldCollider.enabled = false;
            shieldLR.startWidth = 0.02f;
            shieldLR.endWidth = 0.02f;
        }
    }

    void LoadDeathScreen()
    {
        core.StopGame();
        foreach (Transform electron in ElectronParent.transform.GetComponent<Transform>())
        {
            Destroy(electron.gameObject);
        }

        float highscore = PlayerPrefs.GetFloat("highscore");
        bool isNewHighscore = false;
        if (timer > highscore)
        {
            PlayerPrefs.SetFloat("highscore", timer);
            isNewHighscore = true;
            highscore = timer;
        }
        UIManager.GetInstance().LoadDeathScreen(timer, highscore, isNewHighscore);
    }

    void GenerateElectron()
    {
        Vector2 randomSpaawnPoint = Random.insideUnitCircle.normalized;
        GameObject electron = Instantiate(ElectronPrefab, electronSpawnRadius * randomSpaawnPoint, Quaternion.identity);
        electron.transform.SetParent(ElectronParent.transform);
    }

    void MuteSound()
    {
        UIManager.GetInstance().MuteSound();
    }

    private void OnEnable()
    {
        _controls.Action.Enable();
    }

    private void OnDisable()
    {
        _controls.Action.Disable();
    }
}
