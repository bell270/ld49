using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    bool exist = true;
    [SerializeField]
    ParticleSystem damageParticles;
    [SerializeField]
    ParticleSystem chargeParticles;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "OuterAtom" && exist)
        {
            Player.GetInstance().Damage();
            Disolve(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(exist && collision.gameObject.tag == "Player")
        {
            exist = false;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            Player.GetInstance().AddCharge();
            Disolve(false);
        }
    }

    void Disolve(bool damaged)
    {
        StartCoroutine(DisolvingTo(damaged));
    }

    IEnumerator DisolvingTo(bool damaged)
    {
        GetComponent<SpriteRenderer>().enabled = false;
        if (damaged)
        {
            AudioManager.GetInstance().PlayClip("sh",0.8f);
            damageParticles.Play();
            while (damageParticles.isPlaying)
            {
                yield return null;
            }
        }
        else
        {
            AudioManager.GetInstance().PlayClip("charge", 0.8f);
            chargeParticles.Play();
            while (chargeParticles.isPlaying)
            {
                yield return null;
            }
        }
        Destroy(gameObject);
    }
}
