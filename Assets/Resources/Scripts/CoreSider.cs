using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreSider : MonoBehaviour
{
    public float largeRadius = 2; 
    public float angle = 0;
    public float speedFactor = 1;
    float curAngle;
    float e = 0.9f;
    float fixTimer = 0.1f;

    private void Start()
    {
        curAngle = angle * Mathf.Deg2Rad;
    }
    void Update()
    {   
        if(fixTimer > 0f)
        {
            fixTimer -= Time.deltaTime;
            if (fixTimer < 0f)
            {
                TrailRenderer tr = GetComponent<TrailRenderer>();
                tr.time = 0.95f * speedFactor;
            }
        }
        float smallRadius = Mathf.Sqrt(Mathf.Pow(largeRadius, 2) * (1 - Mathf.Pow(e, 2)));
        curAngle += ((2 * Mathf.PI) / speedFactor) * Time.deltaTime;
        float x = Mathf.Cos(curAngle) * largeRadius; 
        float y = Mathf.Sin(curAngle) * smallRadius;

        float x_e = x * Mathf.Cos(angle * Mathf.Deg2Rad) - y * Mathf.Sin(angle * Mathf.Deg2Rad);
        float y_e = x * Mathf.Sin(angle * Mathf.Deg2Rad) + y * Mathf.Cos(angle * Mathf.Deg2Rad);
        transform.position = new Vector2(x_e, y_e);
    }
}
