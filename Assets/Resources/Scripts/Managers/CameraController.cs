using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float zoomGhost = 0f;
    public float zoomDroid = 0f;
    public float zoomSpeed = 1f;
    public float faultMultiplier = 2f;

    private Camera _camera;

    private static CameraController instance;

    public static CameraController GetInstance()
    {
        return instance;
    }
    // Start is called before the first frame update
    void Start()
    {
        if(instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;

        _camera = gameObject.GetComponent<Camera>();
        _camera.orthographicSize = zoomGhost;
    }

    public void GhostCamera()
    {
        StartCoroutine(MoveCamera(zoomGhost, zoomSpeed));
    }

    public void DroidCamera()
    {
        StartCoroutine(MoveCamera(zoomDroid, zoomSpeed));
    }

    public void FaultyCamera(bool isGhost)
    {
        if (isGhost)
        {
            _camera.orthographicSize = zoomDroid;
            StartCoroutine(MoveCamera(zoomGhost, faultMultiplier * zoomSpeed));
        }
        if (!isGhost)
        {
            _camera.orthographicSize = zoomGhost;
            StartCoroutine(MoveCamera(zoomDroid, faultMultiplier * zoomSpeed));
        }
    }

    IEnumerator MoveCamera(float size, float speed)
    {
        float sizeDiff = size - _camera.orthographicSize;
        int sign = sizeDiff > 0 ? 1 : -1;
        while (sign * sizeDiff > 0)
        {
            _camera.orthographicSize += sign * size * Time.deltaTime * speed;
            sizeDiff -= sign * size * Time.deltaTime * speed;
            yield return null;
        }
        _camera.orthographicSize = size;
    }

    public void CameraShake()
    {
        StartCoroutine(Shake(zoomDroid, zoomGhost, faultMultiplier * zoomSpeed));
    }

    public IEnumerator Shake(float sizeMin, float sizeMax, float speed)
    {
        for (int i = 0; i < 3; i++)
        {
            float sizeDiff = sizeMax - _camera.orthographicSize;
            int sign = sizeDiff > 0 ? 1 : -1;
            while (sign * sizeDiff > 0)
            {
                _camera.orthographicSize += sign * sizeMax * Time.deltaTime * speed;
                sizeDiff -= sign * sizeMax * Time.deltaTime * speed;
                yield return null;
            }
            _camera.orthographicSize = sizeMax;
            sizeDiff = sizeMin - _camera.orthographicSize;
            sign = sizeDiff > 0 ? 1 : -1;
            while (sign * sizeDiff > 0)
            {
                _camera.orthographicSize += sign * sizeMin * Time.deltaTime * speed;
                sizeDiff -= sign * sizeMin * Time.deltaTime * speed;
                yield return null;
            }
            _camera.orthographicSize = sizeMin;
        }
    }
}
