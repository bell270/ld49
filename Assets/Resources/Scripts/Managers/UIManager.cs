using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class UIManager : MonoBehaviour
{
    [Header("Main")]
    [SerializeField]
    GameObject startMenu;
    [SerializeField]
    GameObject gameUi;
    [SerializeField]
    Animator fadeAnimator;
    [SerializeField]
    AudioMixer audioMixer;

    [Header("Game UI")]
    [SerializeField]
    TextMeshProUGUI timerUI;

    [Header("Death Screen")]
    [SerializeField]
    GameObject DeathScreen;
    [SerializeField]
    Animator DeathScreenAnimator;
    [SerializeField]
    GameObject DeathTextBlock;
    [SerializeField]
    TextMeshProUGUI ResultText;
    [SerializeField]
    TextMeshProUGUI HighscoreText;

    private static UIManager instance;

    public static UIManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    void Start()
    {
        startMenu.SetActive(true);
        gameUi.SetActive(false);
        DeathScreen.SetActive(false);
        DeathTextBlock.SetActive(false);
    }

    void Update()
    {
    }

    public void StartGame()
    {
        startMenu.SetActive(false);
        gameUi.SetActive(true);
    }

    public void QuitGame()
    {
        Debug.Log("QUITTING GAME...");
        AudioManager am = AudioManager.GetInstance();
        am.StopAll();
        fadeAnimator.SetBool("Faded", true);
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void SetTimer(float timer)
    {
        timerUI.text = timer.ToString("00.00");
    }

    public void LoadDeathScreen(float result, float highscore=0f, bool isNewHighscore=false)
    {
        StartCoroutine(DeathScreenLoading(result, highscore, isNewHighscore));
    }

    public void UnloadDeathScreen()
    {
        StartCoroutine(DeathScreenUnloading());
    }

    IEnumerator DeathScreenLoading(float result, float highscore, bool isNewHighscore)
    {
        AudioManager.GetInstance().StopAll(leaveUnpausable:true);
        AudioManager.GetInstance().PlayClip("death");
        DeathScreen.SetActive(true);
        DeathScreenAnimator.SetBool("Faded", true);
        yield return new WaitForSeconds(1f);
        ResultText.text = "You kept nucleus stable\nfor " + result.ToString("00.00") + " seconds";
        HighscoreText.text = (isNewHighscore ? "NEW " : "") + "HIGHSCORE " + highscore.ToString("00.00") + "s";
        DeathTextBlock.SetActive(true);
        startMenu.SetActive(true);
        gameUi.SetActive(false);
    }

    IEnumerator DeathScreenUnloading()
    {
        DeathTextBlock.SetActive(false);
        DeathScreenAnimator.SetBool("Faded", false);
        yield return new WaitForSeconds(1f);
        startMenu.SetActive(true);
        gameUi.SetActive(false);
        DeathScreen.SetActive(false);
    }

    public void MuteSound()
    {
        float volume = 0f;
        audioMixer.GetFloat("masterVolume", out volume);
        volume = Mathf.Abs(volume) - 80;
        audioMixer.SetFloat("masterVolume", volume);
    }
}
