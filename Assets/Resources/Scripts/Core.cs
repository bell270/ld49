using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Core : MonoBehaviour
{
    bool isPlaying = false;

    [Header("Preferences")]
    [VerticalGroup("Preferences")]
    [SerializeField]
    float emissionSpeed;
    [VerticalGroup("Preferences")]
    [SerializeField]
    float emissionSpeedStart = 5f;
    [VerticalGroup("Preferences")]
    [SerializeField]
    float emissionSpeedLimit = 1f;
    [VerticalGroup("Preferences")]
    float particleSpeed = 80f;
    [VerticalGroup("Preferences")]
    [SerializeField]
    float particleSpeedStart = 80f;
    [VerticalGroup("Preferences")]
    [SerializeField]
    float particleSpeedLimit = 150f;
    [VerticalGroup("Preferences")]
    float emissionTimer;
    [VerticalGroup("Preferences")]
    [SerializeField]
    int emittedCount;


    [VerticalGroup("Emitters")]
    [SerializeField]
    List<Transform> Emitters;

    [VerticalGroup("Contamination")]
    bool isContminated = false;
    float contminationLevel;
    [SerializeField]
    Color contminationColor = new Color(.85f, .18f, .18f, 1f);

    [VerticalGroup("Visuals")]
    Animator animator;

    [VerticalGroup("Prefab")]
    [SerializeField]
    GameObject particlePrefab;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            emissionTimer += Time.deltaTime;
            if (emissionTimer > emissionSpeed)
            {
                emittedCount++;
                emissionTimer = emissionSpeed * Random.Range(-0.1f, 0.1f);
                emissionSpeed = (float)((emissionSpeedStart - emissionSpeedLimit) / 3f * (-Mathf.Atan((emittedCount - 50) / 30) + 2.5));
                particleSpeed = (float)((particleSpeedLimit - particleSpeedStart) / 3f * (Mathf.Atan((emittedCount - 50) / 7) + 1.5)) + particleSpeedStart;
                StartEmition();
            }
            if (isContminated)
            {
                isContminated = false;
                Color newColor = Color.white - contminationLevel * (Color.white - contminationColor);
                newColor.a = 1f;
                GetComponent<SpriteRenderer>().color = newColor;
            }
        }
    }

    public void StartGame()
    {
        emissionTimer = 0f;
        emittedCount = 0;
        emissionSpeed = emissionSpeedStart;
        particleSpeed = particleSpeedStart;
        GetComponent<SpriteRenderer>().color = Color.white;
        isPlaying = true;
    }

    public void StopGame()
    {
        isPlaying = false;
        emissionTimer = 0f;

        StopAllCoroutines();
        foreach(Transform particle in GetComponentInChildren<Transform>())
        {
            if (particle.CompareTag("Particle"))
            {
                Destroy(particle.gameObject);
            }
        }
        emittedCount = 0;
        contminationLevel = 0;
        emissionSpeed = emissionSpeedStart;
        particleSpeed = particleSpeedStart;
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    void StartEmition(bool fullEmission = false)
    {
        if (!fullEmission)
        {
            int emitterId = Random.Range(0, 6);
            animator.SetBool("isEmitting", true);
            StartCoroutine(Emit(emitterId));
        }
        else
        {
            for (int emitterId = 0; emitterId < 6; emitterId++) { 
                StartCoroutine(Emit(emitterId)); 
            }
        }
    }

    IEnumerator Emit(int id)
    {
        AudioManager.GetInstance().PlayClip("emit");
        ParticleSystem emitter = Emitters[id].GetComponent<ParticleSystem>();
        ParticleSystem.MainModule settings = emitter.main;
        settings.startColor = new ParticleSystem.MinMaxGradient(GetComponent<SpriteRenderer>().color);
        emitter.Play();
        while (emitter.isPlaying)
        {
            yield return null;
        }
        animator.SetBool("isEmitting", false);

        float angle = -60 * id;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        GameObject particle = Instantiate(particlePrefab, emitter.transform.position, rotation);
        particle.transform.SetParent(gameObject.transform);
        particle.GetComponent<Rigidbody2D>().AddForce(particleSpeed * emitter.transform.position.normalized);
    }

    public void Contaminate(float contamination)
    {
        isContminated = true;
        contminationLevel = contamination;
    }

    public void Irradiate()
    {

        if (isPlaying)
        {
            StartEmition(true);
        }
    }
}
