using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Electron : MonoBehaviour
{
    [SerializeField]
    Vector3 gravityCenter = Vector3.zero;
    [SerializeField]
    float gravitationalConstant = 6.674f;
    [SerializeField]
    float speedModifier = 10f;
    [SerializeField]
    float disolveRadius = 10f;
    float initSpeed;
    [SerializeField]
    ParticleSystem damageParticles;

    [Header("Arrow")]
    [SerializeField]
    GameObject arrow;
    [SerializeField]
    float electronArrowRadius = 5f;
    [SerializeField]
    float arrowRadius = 4.5f;

    Rigidbody2D rb;
    // Start is called before the first frame update
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initSpeed = Random.Range(-10f, 10f);
        //initSpeed = .1f;
        Vector2 gravityDirection = (gravityCenter - transform.position);
        float distance = gravityDirection.magnitude;
        Vector2 velocityDirection = Vector2.Perpendicular(gravityDirection).normalized;
        rb.velocity = velocityDirection * initSpeed / distance / distance;
    }
    private void Update()
    {
        float arrowAngle = Mathf.Atan2(transform.position.y, transform.position.x) * Mathf.Rad2Deg - 90;
        Quaternion arrowRotation = Quaternion.AngleAxis(arrowAngle, Vector3.forward);
        arrow.transform.SetPositionAndRotation(transform.position.normalized * arrowRadius, arrowRotation);

        float distance = Vector2.Distance(gravityCenter, transform.position);
        Color arrowColor = arrow.GetComponent<SpriteRenderer>().color;
        float middle = (disolveRadius + electronArrowRadius) / 2;
        float offset = (disolveRadius - electronArrowRadius) / 2;
        if (electronArrowRadius < distance && distance < disolveRadius)
        {
            arrowColor.a = -1 * ((distance - middle) * (distance - middle) * (distance - middle) * (distance - middle) * (distance - middle) * (distance - middle)) 
                / (offset * offset * offset * offset * offset * offset) + 1;
        }
        else
        {
            arrowColor.a = 0;
        }
        arrow.GetComponent<SpriteRenderer>().color = arrowColor;
    }

    void FixedUpdate()
    {
        Vector2 gravityDirection = (gravityCenter - transform.position);
        float distance = gravityDirection.magnitude;
        if(distance > disolveRadius)
        {
            Destroy(gameObject);
            return;
        }
        float force = speedModifier * Time.fixedDeltaTime * gravitationalConstant / (distance * distance);
        Vector2 velocityDirection = Vector2.Perpendicular(gravityDirection).normalized;
        rb.AddForce(force * (gravityDirection + velocityDirection));
    }

    IEnumerator Disolving()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        AudioManager.GetInstance().PlayClip("core");
        damageParticles.Play();
        while (damageParticles.isPlaying)
        {
            yield return null;
        }
        Destroy(gameObject);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "InnerAtom")
        {
            Core core = collision.gameObject.GetComponent<Core>();
            if(core != null)
            {
                StartCoroutine(Disolving());
                core.Irradiate();
            }
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AudioManager.GetInstance().PlayClip("bump", 0.6f, 1f);
        }
    }
}
